FROM debian:bookworm-20231009-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN \
  apt update \
  && apt install -q -y -o Dpkg::Options::="--force-confnew" software-properties-common python3 python3-dev python3-pip python3-setuptools python3-venv python3-mako python3-bcrypt git wget curl vim whois dos2unix jq bind9-dnsutils build-essential \
  && update-ca-certificates \
  && python3 --version \
  && pip3 --version \
  && rm -fr /var/lib/apt/lists/*

# non-root for security
RUN \
  addgroup pygroup --gid 9000 \
  && adduser pyuser --uid 9000 --shell /bin/bash --gid 9000
USER pyuser

# Sets utf-8 encoding for Python
ENV LANG=C.UTF-8
# Sets language for Ansible/Python
ENV LC_ALL=C.UTF-8
# Turns off writing .pyc files
ENV PYTHONDONTWRITEBYTECODE=1
# Seems to speed things up
ENV PYTHONUNBUFFERED=1

# Create virtual env directory, install venv pip modules
# 'source' is not /bin/sh friendly, use '.' instead
# Markupsafe pinned to avoid module missing error, paramiko included to avoid blowfish error
COPY requirements.txt ./
ENV VIRTUAL_ENV /home/pyuser/.local
RUN \
  echo $HOME \
  && echo $VIRTUAL_ENV \
  && python3 -m venv $VIRTUAL_ENV \
  && . $VIRTUAL_ENV/bin/activate \
  && pip3 list \
  && pip3 install --upgrade setuptools \
  && pip3 install 'resolvelib<0.6.0' 'Markupsafe==1.1.1' requests paramiko \
  && pip3 list \
  && pip3 install -r requirements.txt \
  && pip3 install ansible-core \
  && ansible-galaxy collection install community.general:7.5.0 ansible.posix:1.5.4 community.kubernetes:2.0.1 community.crypto:2.15.1 ansible.netcommon:5.2.0 \
  && pip3 list \
  && pip3 freeze \
  && ansible --version

# IF we had wanted to upgrade setuptools globally
# temporarily set include-system-site-packages to avoid 'User site-packages are not visible in this virtualenv'
#  && echo "include-system-site-packages = true" > $VIRTUAL_ENV/pyvenv.cfg \
#  && pip3 install --upgrade setuptools \
#  && echo "include-system-site-packages = false" > $VIRTUAL_ENV/pyvenv.cfg \

# bin directory from virtualenv placed first, removes need to source venv activate
ENV PATH=/home/pyuser/.local/bin:$PATH

